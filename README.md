# SuperList

Será uma aplicação Web de lista de tarefas.

## User Stories

1. Eu, Usuário, desejo cadastrar tarefas numa lista, para poder me lembrar dos meus afazeres;
2. Eu, Usuário, desejo voltar a essa lista e ver as tarefas cadastradas anteriormente;
3. Eu, Usuário, desejo poder marcar tarefas como prontas para poder visualizar quais tarefas eu já realizei;
4. Eu, Usuário, desejo poder remover tarefas da lista para que tarefas erradas ou que se tornaram irrelevantes não atrapalhem a visualização das tarefas a serem realizadas; 
5. Eu, Usuário2, desejo cadastrar tarefs numa lista nova, sem interferir em listas previamente cadastradas;
6. Eu, Usuário2, desejo poder voltar à minha lista de tarefas;
7. Eu, Usuário, desejo poder inserir tarefas em uma outra lista, independente das outras já cadastradas;
8. Eu, Usuário, desejo nomear as minhas listas para que eu possa agrupar tarefas relacionadas;
9. Eu, Usuário, desejo ter listas privativas para que eu possa cadastrar tarefas confidenciais;