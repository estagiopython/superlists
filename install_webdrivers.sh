#!/usr/bin/env bash

wget https://github.com/mozilla/geckodriver/releases/download/v0.21.0/geckodriver-v0.21.0-linux64.tar.gz
wget https://chromedriver.storage.googleapis.com/2.40/chromedriver_linux64.zip

tar -xvzf geckodriver-v0.21.0-linux64.tar.gz
unzip chromedriver_linux64.zip

chmod +x geckodriver
chmod +x chromedriver

sudo mv geckodriver ~/.local/bin/geckodriver
sudo mv chromedriver ~/.local/bin/chromedriver